import { Component } from '@angular/core';
import { Post } from './model/post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blog';

  posts: Post[] = [
    new Post('Troisième post de ce magnifique blog', 'Pour la gloire hé hé pour la gloire' , new Date('2011-10-31T10:30:30Z')),
    new Post('Second post de ce magnifique blog', 'it\'s raining today again' , new Date('2011-10-31T10:20:30Z')),
    new Post('Premier post de ce magnifique blog', 'it\'s raining today', new Date('2019-10-29T10:20:30Z')),
];

constructor() { }

}
